using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controlador : MonoBehaviour{

    public float speed;
    public float jampForse;
    private int punts;
    public Text text;
    public Text win;
    private bool esAterra = true;

    void Start(){
        punts = 0;
        updateCounter();
        win.text = "";
    }

    
    void FixedUpdate(){
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float jamp = 0;

        if(esAterra){
            if(Input.GetButtonDown("Jump")){
                jamp = Input.GetAxis("Jump");
            }
        }

        if(Input.GetButtonDown("Jump")){
            esAterra = false;
        }

        Vector3 direction = new Vector3(horizontal, jamp * jampForse , vertical);

        GetComponent<Rigidbody>().AddForce(direction * speed);

    }

    void OnTriggerEnter(Collider other){
        if(other.tag == "item"){
            Destroy(other.gameObject);
            punts++;
            updateCounter();
        }
        if(other.tag == "suelo"){
            esAterra = true;
        }
    }

    void updateCounter(){
        text.text = "Puntos: " + punts;
        int numitems = GameObject.FindGameObjectsWithTag("item").Length;
        if (numitems == 1){
            win.text = "you win";
        }
    }
    
}
